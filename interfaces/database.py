import configparser
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS
import time, datetime
from utils.tools import print_time
from config.auxconfig import CONFIG_FILE

class Influx():

    def __init__(self):

        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)

        # influxdb configuration
        self.influx_url = config['influx2']['url']
        self.influx_org = config['influx2']['org']
        self.influx_token = config['influx2']['token']

        # starting info
        ts = datetime.datetime.now().timestamp()
        ts_converted = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
        print(f'{ts_converted}: Starting InfluxDB client, please wait...')

    def ingest_influx(self, bucket, df, measurement, tags):
            
        # write data into influx
        try:
            with InfluxDBClient(url=self.influx_url, \
                                token=self.influx_token, \
                                org=self.influx_org) \
                                as _client:

                with _client.write_api(write_options=SYNCHRONOUS) as _write_client:
                    # write pandas dataframe
                    _data_frame = df

                    try:
                        _write_client.write(bucket, 
                                            self.influx_org, 
                                            record=_data_frame, 
                                            data_frame_measurement_name=measurement,
                                            data_frame_tag_columns=tags
                                           )
                        arg = f'Data ingest successfully done in bucket {bucket} of Influx OSS 2.0.'
                        print_time(arg=arg)
                        _client.close()  
                        return None
                    except Exception:
                        arg = 'ERROR: Connection error while trying to connect to InfluxDB OSS 2.0.'
                        print_time(arg=arg)
                        _client.close()
                        return None

        except ConnectionError as c:
            print(c)