import pandas as pd
import configparser
import http.client
import json
from colorama import Fore
from json import JSONDecodeError
from utils.tools import print_time, clean_quotes_columns
from config.auxconfig import JSON_RESPONSE_KEY, JSON_RESULT_KEY, CONFIG_FILE

class YahooFinance():

    def __init__(self):
        # starting info
        print_time(arg='Starting YahooFinance API, please wait...')

        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)

        # yahoo finance api configuration
        self.api_host = config['yahoofinance']['host']
        self.url_quotes = config['yahoofinance']['url_quotes']
        self.url_history = config['yahoofinance']['url_history']
        self.url_chart = config['yahoofinance']['url_chart']

        # define the types of markets
        self.markets = ['CRYPTOCURRENCY',
                        'CURRENCY',
                        'FUTURE',
                        'INDEX',
                        'EQUITY',
                        ]

        # http client symbols conversion
        self.convert_symbols = {
                                ',': "%2C",
                                '=': "%3D",
                                '^': "%5E"
                                }

        # get the path of docker secrets
        self.secret_path = config['docker_secrets']['path']

        # get the name of the docker secret
        yahoofinance_api_key = config['docker_secrets']['yahoofinance_api_key']
        self.api_key = self.get_secret(yahoofinance_api_key)  

    def get_secret(self, secret):
        """
        Get a defined secret from Docker secrets. 
        Used on this class initialization.
        """

        try:
            with open(f'{self.secret_path}{secret}', 'r') as secret_file:
                return secret_file.read()
        except IOError:
            print_time(arg=Fore.RED + f"ERROR: It was not possible to get the secret {secret}" + Fore.RESET)
            return None
            
    def get_quotes(self, stocks):
        
        # take a list of the stocks
        stocks_list = stocks['TICKER'].tolist()

        # prepare the request arguments
            # get all the stocks to be passed
        query_symbols = ''
        for i in range(len(stocks_list)):
            if stocks_list[i] != stocks_list[-1]:
                query_symbols = query_symbols + stocks_list[i] + ','
            else:
                query_symbols = query_symbols + stocks_list[i]

            #modify some symbols for the http client
        query_symbols = query_symbols.replace(",", self.convert_symbols[','])
        query_symbols = query_symbols.replace("=", self.convert_symbols['='])
        query_symbols = query_symbols.replace("^", self.convert_symbols['^'])

            # get the url
        url = self.url_quotes + query_symbols

        headers = {
                   'X-API-KEY': self.api_key,
                  }

        # make the get request
        conn = http.client.HTTPSConnection(self.api_host)
        conn.request("GET", url, headers=headers)
        res = conn.getresponse()
        data = res.read()
        text = data.decode("utf-8")

        if res.status == 200:
            try:
                # pass the obtained json to a dataframe
                json_file = json.loads(text)
                json_usable = json_file[JSON_RESPONSE_KEY][JSON_RESULT_KEY]
                df = pd.json_normalize(json_usable)

                # drop the necessary columns to clean the df for each market type
                df_list = []
                for market in self.markets:
                    if len(df) != 0:
                        df_dropped = clean_quotes_columns(df, market)
                        df_list.append((market, df_dropped))
                return json_usable, df_list
            except Exception:
                #arg = 'ERROR trying to decode the JSON file of stocks quotes.'
                print_time(arg='ERROR trying to decode the JSON file of stocks quotes.')
                return None
        else:
            arg = f'An ERROR {res.status} occured during the quotes request to YahooFinance.'
            print_time(arg=arg)
            return None

    def get_charts(self, ranges: str, interval: str, ticker: str) -> tuple:
        
        # define request parameters
        ticker_request = ticker.replace("^", "%" + "5E")
        url = self.url_chart + ticker_request + "?interval=" + interval + "&range=" + ranges
        headers = {
                   'X-API-KEY': self.api_key,
                  }
        
        # make request
        try:
            conn = http.client.HTTPSConnection(self.api_host)
            conn.request("GET", url, headers=headers)
            res = conn.getresponse()
            data = res.read()
            text = data.decode("utf-8")
        except Exception:
            print_time(arg=Fore.YELLOW + f"There was an error doing the request to the API for the stock {ticker}.")
            return None

        # process request data
        if res.status == 200:
            try:
                json_file = json.loads(text)
                main_keys = json_file['chart']['result'][0].keys()

                if "timestamp" in main_keys and "indicators" in main_keys:
                    print_time(arg=f'Trying to create the dataframe for the ticker {ticker}')
                        # timestamps
                    time_ = json_file['chart']['result'][0]['timestamp']
                        # quotes
                    high_ = json_file['chart']['result'][0]['indicators']['quote'][0]['high']
                    low_ = json_file['chart']['result'][0]['indicators']['quote'][0]['low']
                    open_ = json_file['chart']['result'][0]['indicators']['quote'][0]['open']
                    close_ = json_file['chart']['result'][0]['indicators']['quote'][0]['close']
                    volume_ = json_file['chart']['result'][0]['indicators']['quote'][0]['volume']

                    # create the dataframe and convert datetime
                    df_raw = pd.DataFrame(
                                {
                                'time': time_,
                                'open': open_,
                                'high': high_,
                                'low': low_,
                                'close': close_,
                                'volume': volume_
                                }
                                )
                    df_raw.dropna(inplace=True)

                        # clean wrong prices
                    df_build = self.remove_wrong_prices(df_raw, ticker)

                        # force values types for correct ingest in Influx
                    df = pd.DataFrame()
                    df['time'] = [time for time in df_build['time'].tolist()]
                    df['ticker'] = [ticker for ticker in df_build['ticker'].tolist()]
                    df['open'] = [float(open) for open in df_build['open'].tolist()]
                    df['high'] = [float(high) for high in df_build['high'].tolist()]
                    df['low'] = [float(low) for low in df_build['low'].tolist()]
                    df['close'] = [float(close) for close in df_build['close'].tolist()]
                    df['volume'] = [float(volume) for volume in df_build['volume'].tolist()]
                    
                    # convert date from epoch (in seconds) to local human-readable date
                    df['time'] = pd.to_datetime(df['time'], unit='s')
                    df = df.set_index('time').tz_localize(tz='utc').tz_convert('Europe/Madrid')

                    return df, json_file
                else:
                    return None
            except Exception:
                arg = 'ERROR trying to decode the JSON file for ticker {}.'.format(ticker)
                print_time(arg=arg)
                return None
        else:
            arg = f'An ERROR {res.status} occured during the chart request to YahooFinance.'
            print_time(arg=arg)
            return None
    
    def remove_wrong_prices(self, df: pd.DataFrame, ticker: str) -> pd.DataFrame:

        # get each quotations field and initialize the lists where corrected values will be saved
            # time
        _time = df['time'].tolist()
        time_corrected = []
            # close
        _close = df['close'].tolist()
        close_corrected = []
            # high
        _high = df['high'].tolist()
        high_corrected = []
            # low
        _low = df['low'].tolist()
        low_corrected = []
            # open
        _open = df['open'].tolist()
        open_corrected = []
            # volume
        _volume = df['volume'].tolist()
        volume_corrected = []

        for i in range(len(_time)):
            if _close[i] == _high[i] and _close[i] == _low[i] and _close[i] == _open[i] and _volume[i] == 0:
                pass
            else:
                time_corrected.append(_time[i])
                close_corrected.append(_close[i])
                high_corrected.append(_high[i])
                low_corrected.append(_low[i])
                open_corrected.append(_open[i])
                volume_corrected.append(_volume[i])

        ticker_corrected = [ticker] * len(time_corrected)
        quotations_corrected = {'time': time_corrected,
                                'ticker': ticker_corrected,
                                'open': open_corrected,
                                'high': high_corrected,
                                'low': low_corrected,
                                'close': close_corrected,
                                'volume': volume_corrected
                                }

        df_corrected = pd.DataFrame(quotations_corrected)

        return df_corrected