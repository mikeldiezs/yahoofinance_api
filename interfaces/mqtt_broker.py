import paho.mqtt.client as mqtt
import configparser
from colorama import Fore
from config.auxconfig import CONFIG_FILE
from utils.tools import print_time

class MQTTBroker():

    def __init__(self):

        # initialize configuration
        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)

        # broker configuration
        self.broker_address = config['broker']['ip']
        self.port = int(config['broker']['port'])
        self.client_id = config['broker']['clientid']
        self.keepalive = int(config['broker']['keepalive'])

        # create the client
        self.client = mqtt.Client(client_id=self.client_id, clean_session=True)

        # define callback functions
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect

        # connect to the broker
        self.connect_broker()

        self.connection_flag = False

    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self, client, userdata, flags, rc):

        print_time(arg="Trying to connect to the broker...")

        if rc == 0:
            print_time(arg=Fore.GREEN + "Successful connection to the MQTT broker" + Fore.RESET)
            self.connection_flag = True
        else:
            print_time(arg=Fore.RED + "ERROR: Bad connection with result code "+str(rc) + Fore.RESET)

    def connect_broker(self):
        self.client.connect(self.broker_address, self.port, self.keepalive)
        self.client.loop_start()

    def on_disconnect(self, client, userdata, msg):
        print_time(arg=Fore.RED + "ERROR: Disconnection from the broker." + Fore.RESET)
        self.connect_broker()

    def publish_message(self, topic, message):
        self.client.publish(topic, message, qos=1, retain=False)
        print_time(arg=Fore.RESET + f"A new message on topic {topic} has been published.")
        return None