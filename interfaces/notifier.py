import requests, configparser
from pytz import timezone
from colorama import Fore
from utils.tools import print_time
from config.auxconfig import CONFIG_FILE

class Notifier():

    def __init__(self):
        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)
        self.api_url = config["notifier"]["api_url"]
        self.local_timezone = config["notifier"]["timezone"]

        # docker secrets
            # docker secrets path to get api credentials
        self.secret_path = config['docker_secrets']['path']
            # notifier api key
        mynotifier_key = config['docker_secrets']['secret_mynotifier_key']
        self.api_key = self.get_secret(mynotifier_key)

    def get_secret(self, secret):
        """
        Get a defined secret from Docker secrets. 
        Used on this class initialization.
        """

        try:
            with open(f'{self.secret_path}{secret}', 'r') as secret_file:
                return secret_file.read()
        except IOError:
            print_time(arg=Fore.RED + f"ERROR: It was not possible to get the secret {secret}" + Fore.RESET)
            return None

    def push_service_start(self):

        """
        Send a push notification to all the devices registered in MyNotifier to notify the initialization of the Yahoo Finance API service.
        """

        try:
            requests.post(self.api_url, {
            "apiKey": self.api_key, # This is your own private key
            "message": "SYSTEM: Yahoo Finance API", # Could be anything
            "description": "INFO: The Yahoo Finance API service is initialiazing...", # Optional
            "body": "", # Optional
            "type": "warning", # info, error, warning or success
            "project": "" # Optional. Project ids can be found in project tab <-
            })
        except Exception:
            print_time(arg=Fore.RED + "ERROR: It was not possible to push the notification of service start.")
    
        return None

    def push_crash(self):

        """
        Send a push notification to all the devices registered in MyNotifier to notify the crash of the Yahoo Finance API service..
        """

        try:
            requests.post(self.api_url, {
            "apiKey": self.api_key, # This is your own private key
            "message": "SYSTEM: Yahoo Finance API", # Could be anything
            "description": "ERROR: The Yahoo Finance API service crashed and is going down...", # Optional
            "body": "", # Optional
            "type": "error", # info, error, warning or success
            "project": "" # Optional. Project ids can be found in project tab <-
            })
        except Exception:
            print_time(arg=Fore.RED + "ERROR: It was not possible to push the notification of service crash.")
    
        return None