VERSION=v3.2.3
APP=yahoofinance_api

sudo docker build -t $APP:$VERSION .
sudo docker service create --name $APP --secret mynotifier_key --secret yahoofinance_api_key \
--env-file /home/app_env_var/yahoofinance_api/yahoofinance_api_prod.env \
--mount type=bind,source=/home/app_env_var/yahoofinance_api/stocks_universe,target=/rapidapi-yahoo-finance/stocks/ \
$APP:$VERSION