FROM python:3.8-slim

ENV TZ=Europe/Madrid

RUN mkdir rapidapi-yahoo-finance/

COPY . /rapidapi-yahoo-finance
WORKDIR /rapidapi-yahoo-finance

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install schedule==1.0.0
RUN pip install influxdb-client==1.18.0
RUN pip install pandas==1.2.1
RUN pip install paho-mqtt==1.6.1
RUN pip install requests==2.26.0
RUN pip install colorama==0.4.4
RUN pip install pytz==2021.3

RUN mkdir /rapidapi-yahoo-finance/stocks

CMD ["python", "-u", "./__main__.py"]