CONFIG_FILE = './config/config.ini'

# yahoo finance api json keys
JSON_RESPONSE_KEY = "quoteResponse"
JSON_RESULT_KEY = "result"

# yahoo finance api json keys for topics generation
TOPIC_QUOTE_TYPE_KEY = "quoteType"
TOPIC_REGION_KEY = "region"
TOPIC_SYMBOL_KEY = "symbol"