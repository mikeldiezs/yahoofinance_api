from interfaces.api import YahooFinance
from interfaces.mqtt_broker import MQTTBroker
from interfaces.database import Influx
from interfaces.notifier import Notifier
import time, datetime, json, sys, schedule, os
from datetime import datetime
from colorama import Fore 
import pandas as pd
from utils.tools import print_time
from config.auxconfig import TOPIC_QUOTE_TYPE_KEY, TOPIC_REGION_KEY, TOPIC_SYMBOL_KEY

# load stocks universe
universe = pd.read_csv('./stocks/stocks_universe.txt', delimiter = ",")

# get environment variables
CHART_BUCKET = os.environ.get("CHART_BUCKET", "stock_exchange")
QUOTES_BUCKET = os.environ.get("QUOTES_BUCKET", "quotes")
TOPIC_CHART = os.environ.get("TOPIC_CHART", "STOCK/CHART/")

def call_chart(request_type: str, ranges='60d', interval='15m') -> None:
    """
    Make calls to Yahoo Finance API to get the stock prices.

    ARGS
    ----
      - request_type: Use "TRADING" to get the prices of the stocks to use for trading signals.
      - ranges: "60d" is predefined to get the last 60 days of trading, or use any other valid range for Yahoo Finance API.
      - interval: "15m" is predefined to get the prices on a 15 minutes basis, or use any other valid interval for Yahoo Finance API.
    
    RETURN
    ------
    No returns are provided by this function, but two tasks are carried out:
      - Store the prices in the database
      - Publish the message to the MQTT broker
    """

    # get charts only for equities or for the rest of markets (futures, cryptos, etc)
    if request_type == "TRADING":
        stocks_universe = universe[universe['REQUEST_TYPE'] == 'TRADING']
        tickers = stocks_universe['TICKER'].tolist()  
    elif request_type == "HISTORICAL":
        stocks_universe = universe
        tickers = stocks_universe['TICKER'].tolist()  
    else:
        tickers = []

    # initialize the Yahoo Finance API and Influx instances
    rapidapi = YahooFinance()
    influx = Influx()

    ####################################### WRITE TO THE DATABASE #######################################     
    # get the chart for each ticker
    for ticker in tickers:
        # get the market of the ticker
        stock_df = stocks_universe[stocks_universe['TICKER'] == ticker]
        market = stock_df.get('MARKET').values[0]
            
        # get data from Yahoo Finance API
        processed_response = rapidapi.get_charts(ranges=ranges, interval=interval, ticker=ticker)
        if isinstance(processed_response, tuple):
            df, json_raw  = processed_response
        else:
            df, json_raw  = None, None
        
        # ingest data into Influxdb
        try:
            if df is not None:
                if len(df) != 0:
                    influx.ingest_influx(bucket=CHART_BUCKET, 
                                        df=df, 
                                        measurement=market,
                                        tags=['ticker']
                                        )
            else:
                print_time(arg=Fore.YELLOW + f"The dataframe of the stock {ticker} cannot be processed due to data corruption." + Fore.RESET)
        except Exception:
            print_time(arg=Fore.YELLOW + f'ERROR of data ingest for the stock {ticker} in InfluxDB due to a request error.' + Fore.RESET)

    ####################################### PUBLISH THE DATA TO THE BROKER #######################################
        try:
            if json_raw is not None:
                topic = TOPIC_CHART + json_raw['chart']['result'][0]['meta']['symbol']
                message = json.dumps(json_raw)
                
                while not broker.connection_flag:
                    timer = 5
                    print_time(arg=Fore.RESET + f"Waiting {timer} seconds to send the message on topic {topic} due to issues related to MQTT Broker connection...")
                    time.sleep(timer)

                if broker.connection_flag and request_type == "TRADING":
                    broker.publish_message(topic, message)
            else:
                print_time(arg=Fore.YELLOW + f"The message of the stock {ticker} cannot be published due to data corruption." + Fore.RESET)             
        except Exception:
            print_time(arg=Fore.YELLOW + f"ERROR: The stock prices message of the stock {ticker} could not be published to the broker." + Fore.RESET)

def call_quotes():

    # get the stocks used to get the quotes
    quotes_universe = universe[universe['REQUEST-TYPE'] == 'QUOTES']

    # get data from YahooFinance API
    if len(quotes_universe) > 0:
        rapidapi = YahooFinance()
        api_data = rapidapi.get_quotes(stocks=quotes_universe)
        json_raw = api_data[0]
        df_list = api_data[1]

    # ingest data into Influxdb and publish the data to the broker
    try:
        for i in range(len(df_list)):
            df_to_influx = df_list[i][1]
            measurement = df_list[i][0]
            if len(df_to_influx) != 0:
                influx = Influx()
                influx.ingest_influx(bucket=QUOTES_BUCKET, 
                                     df=df_to_influx, 
                                     measurement=measurement,
                                     tags=['symbol']
                                    )
    except Exception:
        arg = 'ERROR of data ingest for several stocks quotes in InfluxDB due to a request error.'
        print_time(arg=arg)

    # publish the data to the mqtt broker
    try:
        for i in range(len(json_raw)):
            topic = "STOCK/" + json_raw[i][TOPIC_QUOTE_TYPE_KEY] + "/" + json_raw[i][TOPIC_REGION_KEY] + "/" + json_raw[i][TOPIC_SYMBOL_KEY]
            message = json.dumps(json_raw[i])
            broker.publish_message(topic, message)
    except Exception:
        arg = "ERROR: The messages could not be published to the broker."
        print_time(arg=arg)
        return None
    
    return None

if __name__ == '__main__':

    notifier = Notifier()
    notifier.push_service_start()

    # connect to the broker
    broker = MQTTBroker()
    broker.connect_broker()
    while not broker.connection_flag:
        timer = 5
        print_time(arg=Fore.RESET + f"Waiting {timer} seconds for connection to the broker...")
        time.sleep(timer)

    # scheduler for trading signals
    scheduler1 = schedule.Scheduler()
    scheduler1.every().hour.at(":00").do(call_chart, request_type='TRADING')
    scheduler1.every().hour.at(":15").do(call_chart, request_type='TRADING')
    scheduler1.every().hour.at(":30").do(call_chart, request_type='TRADING')
    scheduler1.every().hour.at(":45").do(call_chart, request_type='TRADING')

    # scheduler for history calls of all the stocks
    scheduler2 = schedule.Scheduler()
    scheduler2.every().sunday.at("00:00").do(call_chart, request_type='HISTORICAL')

    # initialize infinite loop to get the data
    while True:

        try:
            # calls from monday to friday
            while datetime.now().weekday() < 5 and 7 <= datetime.now().hour < 23:
                scheduler1.run_pending()
                time.sleep(1)

            # calls on weekend
            while datetime.now().weekday() >= 5:
                scheduler2.run_pending()
                time.sleep(1)
            time.sleep(1)

        except Exception:
            print_time(arg=Fore.RED + "The program crashed. Finishing the service...")
            notifier.push_crash()
            sys.exit(1)
